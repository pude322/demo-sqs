var AWS = require('aws-sdk');
// Create an SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});
exports.handler = async (event, context) => {
    var params = {
        // Remove DelaySeconds parameter and value for FIFO queues
       DelaySeconds: 3,
       MessageAttributes: {
         "Title": {
           DataType: "String",
           StringValue: "The Whistler"
         },
         "Author": {
           DataType: "String",
           StringValue: "John Grisham"
         },
         "WeeksOn": {
           DataType: "Number",
           StringValue: "6"
         }
       },
       MessageBody: "222222",
       // MessageDeduplicationId: "TheWhistler",  // Required for FIFO queues
       // MessageGroupId: "Group1",  // Required for FIFO queues
       QueueUrl: "your sqsurl"
     };

     let promises =[]

     for (let index = 0; index < 20; index++) {
        promises.push(qqq(params))
     }
     

     await Promise.all(promises).then(function (albums) { // albums is 2D array
        albums = [].concat.apply([], albums); // flatten the array
        console.log('albums:',albums);
        //do something with the finalized list of albums here
    });
     
    //  sqs.sendMessage(params, function(err, data) {
    //    if (err) {
    //      console.log("Error", err);
    //    } else {
    //      console.log("Success", data.MessageId);
    //    }
    //  });

}

async function qqq(params) {
    try {
        let data = await sqs.sendMessage(params).promise()
        // console.log("Success", data.MessageId);
        return "Success::"+data.MessageId
     } catch (error) {
        // console.log("Error", error);
        return "Error::"+error.message
     }
    // return new Promise(resolve => setTimeout(resolve, ms));
}